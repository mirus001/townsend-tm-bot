
# To install the Python client library:
# pip install -U selenium

# Import the Selenium 2 namespace (aka "webdriver")
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time

class GGMembershipReader(object):

    
    def retrieveMemberEmails(self, login_creds):
        print("RETRIEVING MEMBER LIST FROM GOOGLE GROUP")
        driver = webdriver.Chrome('./chromedriver')
        email = login_creds['username']
        password = login_creds['password']
        # ------------------------------
        # The actual test scenario: Test the codepad.org code execution service.

        # Go to codepad.org
        #driver.get('https://groups.google.com/forum/#!managemembers/townsend-toastmasters/members/active')
        driver.get('https://accounts.google.com')


        try:
            emailField = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//input[@type='email']"))
            )

            emailField.send_keys(email)
            emailField.send_keys(Keys.RETURN)
            time.sleep(5)
        except:
            print("logout button not found")
            raise
            
        success = False
        count = 0
        while not success:
            try:
                passwordField = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.XPATH, "//input[@type='password']"))
                )

                while not passwordField.is_displayed():
                    print("Cannot see password field")
                    time.sleep(1)

                passwordField.send_keys(password)
                passwordField.send_keys(Keys.RETURN)
                success = True
                count += 1
                if count > 10:
                    raise ("Cannot find password field located by //input[@type='password']")
            except:
                #driver.quit()
                print("Login page not loaded")
                raise

        time.sleep(2)
        driver.get('https://groups.google.com/forum/#!managemembers/townsend-toastmasters/members/active')
        time.sleep(2)

        table = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//table[@class='dataTable']"))
            )

        tableRows = driver.find_elements(By.XPATH, "//table[@class='dataTable']//tr[@class='gdf-tableRow']")
        memberDictionary = {}
        for row in tableRows:
            columns = row.find_elements(By.XPATH, ".//td")
            name = columns[1].find_element(By.XPATH, "div[@class='gwt-Label']")
            emailAddress = columns[3].find_element(By.XPATH, "div[@class='gwt-Label']")
            
            memberDictionary[name.text] = emailAddress.text

        driver.quit()
        return memberDictionary
        
        #return memberDictionary
        

#reader = GGMembershipReader()
#reader.retrieveMemberEmails()
