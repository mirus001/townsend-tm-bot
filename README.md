# README #
This bot will send emails to members of Townsend Toastmasters based on the schedule found in Google Sheets, email information in the TownsendToastmasters Google Group and Toastmasters International.

### What is this repository for? ###

* This tool requires the executing user to be an officer of Townsend Toastmasters to be able to view the [club roster](https://www.toastmasters.org/My-Toastmasters/Profile/Club-Central-Beta/Club-Roster) and the Google Group mailing list
* Version 0.1


### What This Bot Will Do ###

1. This bot will retrieve meeting information from the club's Google Sheets document
2. This bot will retrieve club contact information from the Townsend Toastmasters Google Group
3. This bot will retrieve club contact information from the Toastmasters International page (in case the Google Group mailing list isn't up to date)
4. This bot will generate a meeting agenda containing each member name and their role
5. This bot will send an email with the agenda to each meeting participant and the Vice President, Education

### What This Bot Will Not Do ###

1. This bot will not automatically send out scheduled emails -- you'll need to set up a cron job or run another automated tool (like Jenkins) to call the bot at periodic intervals
2. This bot will not update meeting information in the Google Sheet or any other resource it accesses -- the bot treats every resource it interacts with as read-only

### Getting Started ###

Please make sure that you have the proper credentials to log into the following sites and/or view the following pages:

* Club roster - [https://www.toastmasters.org/My-Toastmasters/Profile/Club-Central-Beta/Club-Roster]
* Meeting Schedule on Google Sheets - https://docs.google.com/spreadsheets/d/1SXPSQEyICVRD1BsOA2TKly1uo0-KZDWEHjwLPtaLyUE/edit#gid=612239181


### How do I get set up? ###

* Install [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
* Install [python 2.7](https://www.python.org/download/releases/2.7/) on your machine
* Install pre-requisites by calling `pip install -r pip_requirements.txt`
* Modify `res/tm_credentials.txt` with your login credentials for Google Sheets and [Toastmasters International](https://www.toastmasters.org/)
* Execute by calling `python scheduler.py` 
* To send emails, call `python schedule.py --send`
* To view command usage, call `python scheduler.py --help`