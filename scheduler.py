from __future__ import print_function
import httplib2
import os
import time
import argparse

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from datetime import datetime
from tmEmailFetcher import MembershipReader
from emailSender import Emailer 
from googleEmailFetcher import GGMembershipReader

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None
    
# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
CLIENT_SECRET_FILE = 'res/client_secret.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def get_toastmaster_credentials():
    creds = {}
    try:
        tm_cred_file = open("res/tm_credentials.txt", "r")
        for line in tm_cred_file:
            if line.startswith('TM_username'):
                creds['username'] = line.split("=")[1].rstrip().lstrip()
            elif line.startswith('TM_password'):
                creds['password'] = line.split("=")[1].rstrip().lstrip()
    except:
        raise Exception("Please add your toastmasters.com login credentials to file \"res/tm_credentials.txt\"")
    return creds

def get_google_credentials():
    creds = {}
    try:
        tm_cred_file = open("res/tm_credentials.txt", "r")
        for line in tm_cred_file:
            if line.startswith('Google_username'):
                creds['username'] = line.split("=")[1].rstrip().lstrip()
            elif line.startswith('Google_password'):
                creds['password'] = line.split("=")[1].rstrip().lstrip()
    except:
        raise Exception("Please add your google account login credentials to file \"res/tm_credentials.txt\"")
    return creds


def main():
    parser = argparse.ArgumentParser(description='Automated TM emailer')
    parser.add_argument('--send', help='send the email', action="store_true")
    parser.add_argument('--date', help='specify simulated date work with... format = YYYY-MM-DD')
    args = parser.parse_args()

    tm_login_dict = get_toastmaster_credentials()
    google_login_dict = get_google_credentials()

    if args.send:
        print ("emails will be sent")
    
    if args.date:
        print (args.date)

    credentials = get_credentials()
    
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

    spreadsheetId = '1SXPSQEyICVRD1BsOA2TKly1uo0-KZDWEHjwLPtaLyUE'
    participantRange = 'Q2-2017!A2:L14'
    participantResult = service.spreadsheets().values().get(
        spreadsheetId=spreadsheetId, range=participantRange).execute()
    participantNames = participantResult.get('values', [])
    if(args.date):
        currentTime = datetime.strptime(args.date, '%Y-%m-%d')
    else:
        currentTime = datetime.now()

    roleRange = 'Q2-2017!A1:L1'
    roleData = participantResult = service.spreadsheets().values().get(
        spreadsheetId=spreadsheetId, range=roleRange).execute()
    roleValues = roleData.get('values', [])
    print(roleValues)

    meetingMember = []

    # Retrieve member emails from TMI
    reader = MembershipReader()
    ggReader = GGMembershipReader()
    ggMeetingDictionary = ggReader.retrieveMemberEmails(google_login_dict)
    meetingDictionary = reader.retrieveMemberEmails(tm_login_dict)

    
    if not participantNames:
        print('No data found.')
    else:
        for meetingRow in participantNames:
            meetingDate = meetingRow[0]
            print ("MEETING DATE %s" % meetingDate)

            
            datetime_object = datetime.strptime(meetingDate, '%m/%d/%Y')
            
            if datetime_object > currentTime:
                print (datetime_object)
                print ("This is the meeting")
                # Print columns A and E, which correspond to indices 0 and 4.
                for i in range (1, len(meetingRow)):
                    #print(getEmailAddress(meetingRow[i], meetingDictionary))
                    memberEmail = getEmailAddress(meetingRow[i], ggMeetingDictionary)
                    if len(memberEmail) < 1:
                        print("XXX could not find "+meetingRow[i]+" in googlegroups email list")
                        memberEmail = getEmailAddress(meetingRow[i], meetingDictionary)
                    meetingMember.append(MeetingMember(roleValues[0][i], meetingRow[i], memberEmail))

                for member in meetingMember:
                    print(member.roleName +' : ' +member.memberName+" : "+member.email)
                break
            
            # appendVPEducation
            

    sender = Emailer()
    emailRecipients = ""
    emailAddys = []
    meetingMember.append(MeetingMember("VPE", getEmailAddress("VPE-name", meetingDictionary), getEmailAddress("VPE-email", meetingDictionary)))

    for member in meetingMember:
        if len(member.email) > 0:
            print("joining email "+member.email+" "+member.memberName+" (role "+member.roleName+")")
            emailRecipients+=(", "+member.email)
            emailAddys.append(member.email)

    print ("email to string: "+emailRecipients)

    ### SAFETY ###
    if(args.send):
        sender.sendEmail("townsendtoastmasters@gmail.com", emailAddys, meetingMember, datetime_object)
    else:
        print("Emails not sent! Specify --send to send emails.")
    
def getEmailAddress(memberName, clubDictionary):
    if len(memberName) == 0:
        return ""
    value = "NO MATCH FOUND"
    try:
        value = next(v for (k,v) in clubDictionary.iteritems() if memberName in k)
    except:
        names = memberName.split()
        for (k,v) in clubDictionary.iteritems():
            # does the member name match the string name?
            for eachName in names:
                matchFound = True
                if eachName.upper() not in k.upper():
                    matchFound = False
            # no match found? maybe the name is in the email
            if matchFound == False:
                for eachName in names:
                    matchFound = True
                    if eachName.upper() not in v.upper():
                        matchFound = False
            if matchFound == True:
                value = v
    if value is "NO MATCH FOUND":
        print ("No email address found for member "+memberName)
        value = ""
    return value



class MeetingMember(object):
    def __init__(self, roleName, memberName, email):
        self.roleName = roleName
        self.memberName = memberName
        self.email = email

if __name__ == '__main__':
    main()
