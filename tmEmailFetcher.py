
# To install the Python client library:
# pip install -U selenium

# Import the Selenium 2 namespace (aka "webdriver")
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class MembershipReader(object):

    
    def retrieveMemberEmails(self, tm_login_creds):
        print("RETRIEVING MEMBER LIST FROM TOASTMASTERS INTERNATIONAL WEBSITE")
        driver = webdriver.Chrome('./chromedriver')
        email = tm_login_creds['username']
        password = tm_login_creds['password']
        # ------------------------------
        # The actual test scenario: Test the codepad.org code execution service.

        # Go to codepad.org
        driver.get('https://www.toastmasters.org/')


        try:
            logoutButton = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//a[@id='navItem_0'][contains(text(), 'Logout')]"))
            )
            
        except:
            print("logout button not found")
            
        try:
            loginButton = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//a[@id='navItem_0'][contains(text(), 'Login')]"))
            )
            
        except:
            print("login button not found")

            
        loginButton.click()

        try:
            emailField = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.ID, "UserName"))
            )

            passwordField = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.ID, "Password"))
            )

            signinButton = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//input[@value='Log In']"))
            )
            
        except:
            #driver.quit()
            print("Login page not loaded")

        emailField.send_keys(email)
        passwordField.send_keys(password)
        signinButton.click()

        try:
            logoutButton = WebDriverWait(driver, 90).until(
                EC.presence_of_element_located((By.XPATH, "//a[@id='navItem_0'][contains(text(), 'Logout')]"))
            )
            
        except:
            print("logout button not found... login unsuccessful")
            raise Exception("Login to Toastmasters Roster Unsucessful")

        driver.get('https://www.toastmasters.org/My-Toastmasters/Profile/Club-Central-Beta/Club-Roster')


        try:
            townsendTMLink = WebDriverWait(driver, 90).until(
                EC.presence_of_element_located((By.XPATH, "//h1[@class='title'][contains(text(), 'Club Roster')]"))
            )
            
        except:
            print("Townsend Toastmasters Officer Admin Link not Found")


        try:
            membersTable = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//table[@id='tblData']//tr"))
            )

            
        finally:
            #driver.quit()
            print("member tablefound")

        #Member match: //table[@id='tblData']//tr//td//span[contains(text(), 'Akshay')]

        tableRows = driver.find_elements(By.XPATH, "//table[@id='tblData']//tr")

        
        memberDictionary = {}
        #print ("length of tableRows = "+str(len(tableRows)))
        for row in tableRows:
            columns = row.find_elements(By.XPATH, ".//td")
            emailAddress = columns[3].find_element(By.XPATH, "a[contains(text(), '@')]")
            name = columns[0].find_element(By.XPATH, "span[@class='memberName']")
            if "Vice President Education" in columns[0].text:
                print (name.text+" is the vpe!!!")
                memberDictionary["VPE-email"] = emailAddress.text
                memberDictionary["VPE-name"] = name.text
            memberDictionary[name.text] = emailAddress.text
            
        driver.quit()
        return memberDictionary
        

#reader = MembershipReader()
#reader.retrieveMemberEmails()
