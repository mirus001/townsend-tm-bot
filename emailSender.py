
import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime
from time import sleep
#from scheduler import MeetingMember

class Emailer(object):

    
    def sendEmail(self, user, recipient, meetingRoles, meetingDate):
        roles = ["Toastmaster_Name", 
        "Speaker_1_Name", "Evaluator_1_Name",
        "Speaker_2_Name", "Evaluator_2_Name",
        "Speaker_3_Name", "Evaluator_3_Name", 
        "Table_Topics_Master_Name",
        "General_Evaluator_Name",
        "Grammarian_Um_Counter_Name",
        "Timer_Name",
        "VPE"
        ]

        gmail_user = 'townsendtoastmasters@gmail.com'
        gmail_pwd = "600TownsendLaw"    
        FROM = user
        TO = recipient 

        formattedDate = meetingDate.strftime('%B %d')
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "[Townsend Toastmasters] Reminder: You have a role on "+formattedDate
        msg['From'] = user
        msg['To'] = ", ".join(recipient)


        # Create the body of the message (a plain-text and an HTML version).
        text = "Hi!\nHow are you?\nHere is the link you wanted:\nhttp://www.python.org"
        with open('res/template.html', 'r') as myfile:
            html=myfile.read().replace('\n', '')

        
        for i in range (0, len(roles)):
            print("replacing "+roles[i]+" "+meetingRoles[i].memberName)
            if('Toastmaster' in meetingRoles[i].roleName):
                print("replacing toastmaster email: "+meetingRoles[i].email)
                html = html.replace('Toastmaster_Email', meetingRoles[i].email)   
            if('VPE' in meetingRoles[i].roleName):
                print("replacing vpe info with: "+meetingRoles[i].memberName+" & "+meetingRoles[i].email)
                html = html.replace('VPE_Email', meetingRoles[i].email) 
                html = html.replace('VPE_Name', meetingRoles[i].memberName) 
            else:
                html = html.replace(roles[i], meetingRoles[i].memberName)
            


        html = html.replace('Meeting_Date', formattedDate)


        #print(html)

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        msg.attach(part1)
        msg.attach(part2)

        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        print ("size "+str(len(recipient))+" "+str(recipient))
        server.sendmail('townsendtoastmasters@gmail.com', TO, msg.as_string())
        sleep(2)
    
        server.close()
            


#sender = Emailer()

#sender.sendEmail("townsend_bot@no-reply.com", "mirus001@gmail.com")